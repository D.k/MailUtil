# MailUtil

- 这是一个轻量级的Java Swing 邮件发送工具，由于工作需要开发(Swing处女座作品:joy:)
- 该工具目前主要功能扫描指定目录下最新HTML文件并发送,超过20人则每20人发送一次并休眠40s
- 工具采用Quartz作为定时任务,Quartz全部默认配置，包括任务存储在RAM(后期考虑SQLite)
- 工具暂时不支持发送附件(代码中有点，但实际业务不需要，故未添加)
- 工具界面样式来源于开源项目 beautyeye(感谢:pray:)
- 项目采用Maven构建，建议Maven切换[开源中国Maven仓库](http://maven.oschina.net/help.html)
- _开源中国Maven仓库中上传的beautyeye.jar有些问题，故需要手动安装jar到本地仓库_
- 项目默认环境为JDK1.6，编码为UTF-8，使用Eclipse开发，Swing界面采用插件[Windowbuilder](https://eclipse.org/windowbuilder/)绘制
- 项目API存放于 MailUtil/doc/index.html(更新不一定及时)
- 添加了Quartz采用的Cron表达式生成工具(位于crontool目录) 感谢[废柴大叔](http://my.oschina.net/ij2ee)

### 项目依赖
1. [UI元素 beautyeye](http://git.oschina.net/jackjiang/beautyeye) 
1. [邮件发送工具](http://www.xdemo.org/java-mail/)
1. [浏览器调用](http://my.oschina.net/u/862250/blog/91777)
1. [Quartz](https://quartz-scheduler.org)

### 安装beautyeye到本地Maven仓库
1. 首先下载[beautyeye](https://github.com/JackJiang2011/beautyeye/archive/3.5.zip)
2. 将其解压到任意位置
3. 配置好Maven后执行如下命令

```
mvn install:install-file -Dfile=解压位置/dist/beautyeye_lnf.jar 
-DgroupId=org.jb2011.lnf.beautyeye -DartifactId=beautyeye_lnf 
-Dversion=3.6 -Dpackaging=jar
```

### 打包可执行jar

```
mvn assembly:assembly
```

### 工具截图
![主界面](http://git.oschina.net/uploads/images/2015/1104/225533_c289dcb9_111846.png "主界面")
![导入导出配置文件](http://git.oschina.net/uploads/images/2015/1104/225553_5fabc683_111846.png "导入导出配置文件")
![错误提示](http://git.oschina.net/uploads/images/2015/1104/225614_7a62bc77_111846.png "错误提示")
![定时发送](http://git.oschina.net/uploads/images/2015/1104/225642_e14b67a4_111846.png "定时发送")
![定时发送错误提醒](http://git.oschina.net/uploads/images/2015/1104/225712_ef7eadf1_111846.png "定时发送错误提醒")
![配置锁定](http://git.oschina.net/uploads/images/2015/1104/225740_320cf782_111846.png "配置锁定")

### 其他相关
1. Quartz资料 [Daniel博客](http://blog.danielguo.xyz/archive.html)
2. Cron表达式 [田文博客](http://blog.csdn.net/tiwen818/article/details/6988105)

### 后期完善&&已知BUG
- 使用SqLite存储相关信息，包括Quartz任务信息
- 定时发送失败自动发送失败信息到指定邮箱