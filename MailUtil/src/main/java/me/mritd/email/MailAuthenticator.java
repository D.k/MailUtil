package me.mritd.email;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * 
 * @ClassName MailAuthenticator
 * @Description TODO 自定义认证类
 * @author 漠然 mritd@mritd.me
 * @date 2015年10月5日 上午9:20:25
 * @see <a href='http://www.xdemo.org/java-mail/'>大飞的xdemo站点<a>
 */
public class MailAuthenticator extends Authenticator{
	String userName = null;
	String password = null;

	public MailAuthenticator() {
	}

	public MailAuthenticator(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	}
}
