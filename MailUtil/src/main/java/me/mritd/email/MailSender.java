package me.mritd.email;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import me.mritd.vo.EmailVo;

import org.apache.log4j.Logger;

/**
 * 
 * @ClassName MailSender
 * @Description TODO 邮件发送主类
 * @author 漠然 mritd@mritd.me
 * @date 2015年10月5日 上午9:24:03
 * @see <a href='http://www.xdemo.org/java-mail/'>大飞的xdemo站点<a>
 */
public class MailSender {
	
	private static final Logger LOGGER = Logger.getLogger(MailSender.class);
	/**
	 * 以文本格式发送邮件
	 * 
	 * @param mail
	 *            待发送的邮件的信息
	 * @throws GeneralSecurityException
	 */
	public boolean sendTextMail(EmailVo mail) throws GeneralSecurityException {
		// 判断是否需要身份认证
		MailAuthenticator authenticator = null;
		Properties pro = mail.getProperties();
		if (mail.isValidate()) {
			// 如果需要身份认证，则创建一个密码验证器
			authenticator = new MailAuthenticator(mail.getUserName(),
					mail.getPassword());
		}
		// 根据邮件会话属性和密码验证器构造一个发送邮件的session
		Session sendMailSession = Session
				.getDefaultInstance(pro, authenticator);
		try {
			// 根据session创建一个邮件消息
			Message mailMessage = new MimeMessage(sendMailSession);
			// 创建邮件发送者地址
			Address from = new InternetAddress(mail.getFromAddress());
			// 设置邮件消息的发送者
			mailMessage.setFrom(from);
			// 创建邮件的接收者地址，并设置到邮件消息中
			Address to = new InternetAddress(mail.getToAddress());
			mailMessage.setRecipient(Message.RecipientType.TO, to);
			// 设置邮件消息的主题
			mailMessage.setSubject(mail.getSubject());
			// 设置邮件消息发送的时间
			mailMessage.setSentDate(new Date());
			// 设置邮件消息的主要内容
			String mailContent = mail.getContent();
			mailMessage.setText(mailContent);
			// 发送邮件
			Transport.send(mailMessage);
			return true;
		} catch (MessagingException ex) {
			LOGGER.error("异常信息: ", ex);
		}
		return false;
	}

	/**
	 * 以HTML格式发送邮件
	 * 
	 * @param mail
	 *            待发送的邮件信息
	 * @throws GeneralSecurityException
	 * @throws UnsupportedEncodingException
	 */
	public boolean sendHtmlMail(EmailVo mail) {
		LOGGER.info("######  以HTML形式发送邮件开始......");
		LOGGER.info("######  MailServerHost:"+mail.getMailServerHost());
		LOGGER.info("######  MailServerPort:"+mail.getMailServerPort());
		LOGGER.info("######  FromAddress:"+mail.getFromAddress());
		LOGGER.info("######  Password:"+mail.getPassword());
		LOGGER.info("######  ToAddress:"+mail.getToAddress());
		LOGGER.info("######  Subject:"+mail.getSubject());
		
		try {
			// 判断是否需要身份认证
			MailAuthenticator authenticator = null;
			Properties props = mail.getProperties();
			// 如果需要身份认证，则创建一个密码验证器
			if (mail.isValidate()) {
				authenticator = new MailAuthenticator(mail.getUserName(),mail.getPassword());
				LOGGER.info("######  创建密码验证器！");
			}
			// 根据邮件会话属性和密码验证器构造一个发送邮件的session
			Session sendMailSession = Session.getDefaultInstance(props,authenticator);
			LOGGER.info("###### 创建  sendMailSession!");
	//		sendMailSession.setDebug(true);
			// 根据session创建一个邮件消息
			Message mailMessage = new MimeMessage(sendMailSession);
			LOGGER.info("###### 创建邮件信息 mailMessage！");
			// 创建邮件发送者地址
			Address from = new InternetAddress(mail.getFromAddress(),
					mail.getFromNickName() == null ? ""
							: mail.getFromNickName());
			LOGGER.info("###### 设置邮件发送者地址!");
			// 设置邮件消息的发送者
			mailMessage.setFrom(from);
			LOGGER.info("###### 设置邮件发送者!");
			// 创建邮件的接收者地址，并设置到邮件消息中,可以设置多个收件人，逗号隔开
			// Message.RecipientType.TO属性表示接收者的类型为TO,CC表示抄送,BCC暗送
			mailMessage.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(mail.getToAddress()));
			LOGGER.info("###### 设置接受者地址!");
			// 设置邮件消息的主题
			mailMessage.setSubject(mail.getSubject());
			LOGGER.info("###### 设置消息主题!");
			// 设置邮件消息发送的时间
			mailMessage.setSentDate(new Date());
			LOGGER.info("###### 设置邮件发送时间!");
			// MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
			Multipart mainPart = new MimeMultipart();
			// 创建一个包含HTML内容的MimeBodyPart
			MimeBodyPart html = new MimeBodyPart();
			// 设置HTML内容
			html.setContent(mail.getContent(), "text/html; charset=utf-8");
			mainPart.addBodyPart(html);
			LOGGER.info("###### 设置邮件HTML内容!");
			// 设置信件的附件(用本地上的文件作为附件)
			FileDataSource fds = null;
			DataHandler dh = null;
			if(mail.getAttachments()!=null){
				for (File file : mail.getAttachments()) {
					html = new MimeBodyPart();
					fds = new FileDataSource(file);
					dh = new DataHandler(fds);
					html.setFileName(file.getName());
					html.setDataHandler(dh);
					mainPart.addBodyPart(html);
				}
			}
			// 将MiniMultipart对象设置为邮件内容
			mailMessage.setContent(mainPart);
			mailMessage.saveChanges();
			// 发送邮件
			LOGGER.info("###### 执行发送!");
			Transport.send(mailMessage);
			return true;
		} catch (MessagingException e) {
			LOGGER.error("######  邮件发送出现异常!");
			LOGGER.error("异常信息: ", e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("######  邮件发送出现异常!");
			LOGGER.error("异常信息: ", e);
		} catch (GeneralSecurityException e) {
			LOGGER.error("######  邮件发送出现异常!");
			LOGGER.error("异常信息: ", e);
		}
		return false;
	}
}
