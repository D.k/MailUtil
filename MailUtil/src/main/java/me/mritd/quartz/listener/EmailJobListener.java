package me.mritd.quartz.listener;

import java.awt.TrayIcon;
import java.io.File;
import java.util.ArrayList;

import me.mritd.quartz.execute.EmailJobExecute;
import me.mritd.util.FileUtil;
import me.mritd.vo.EmailVo;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
/**
 * 
 * Copyright © 2015 Mritd. All rights reserved.
 * @ClassName EmailJobListener
 * @Description TODO 定时邮件任务监听器
 * @author 漠然
 * @date 2015年11月1日 下午8:16:56
 * @version 2.0
 */
public class EmailJobListener implements JobListener{
	
	public static final String LISTENER_NAME = "EmailJobListener";
	private Logger logger = Logger.getLogger(EmailJobListener.class);
	
	@Override
	public String getName() {
		return LISTENER_NAME;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		String jobName = context.getJobDetail().getKey().toString();
		logger.info("###### JobToBeExecuted : "+jobName);
		try {
			logger.info("###### EmailJobListener 开始更新 mainIfo!");
			FileUtil fileUtil = new FileUtil();
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
			EmailVo mailInfo = (EmailVo) dataMap.get("mailInfo");
			//获取文件列表
			ArrayList<File> fileList = fileUtil.getListFiles(mailInfo.getHtmlDir());
			if (fileList==null||fileList.size()==0) {
				logger.error("XXXXXX Listener获取文件列表失败!");
				throw new Exception("Listener获取文件列表失败!");
			}
			//获取目录下最后修改文件
			File lastModifyFile = fileUtil.getLastModifyFile(fileList);
			if (!lastModifyFile.getName().trim().endsWith(".html")) {
				logger.error("XXXXXX 最后修改文件并非HTML文件!");
				throw new Exception("最后修改文件并非HTML文件!");
			}else{
				//读取文件内容
				String contentValue = fileUtil.readFileByLines(lastModifyFile);
				//更新邮件内容
				mailInfo.setContent(contentValue);
				logger.info("###### EmailJobListener 更新 mainIfo成功!");
				logger.info("###### mainIfo 更新 Content: "+contentValue);
				//更新邮件主题
				mailInfo.setSubject(lastModifyFile.getName().substring(0,lastModifyFile.getName().indexOf(".html")));
				logger.info("###### EmailJobListener 更新 mainIfo成功!");
				logger.info("###### mainIfo 更新 Subject: "+lastModifyFile.getName().substring(0,lastModifyFile.getName().indexOf(".html")));
			}
		} catch (Exception e) {
			logger.error("XXXXXX EmailJobListener 更新mailInfo信息失败!");
			//强制删除mailInfo待发送信息 强制终止任务 防止停止任务后当前邮件任务仍会发送
			JobDataMap dataMap = context.getJobDetail().getJobDataMap();
			dataMap.remove("mailInfo");
			logger.info("###### mailInfo已被清空!");
			EmailJobExecute emailJobExecute = new EmailJobExecute();
			boolean stopSendEmail = emailJobExecute.stopSendEmail();
			if (stopSendEmail) logger.info("###### 停止定时邮件任务成功!");
			else logger.info("###### 停止定时邮件任务失败!");
			logger.error("异常信息 : ", e);
		}
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context,
			JobExecutionException jobException) {
		String jobName = context.getJobDetail().getKey().toString();
		logger.info("###### Job Execut Finished : "+jobName);
		TrayIcon trayIcon = (TrayIcon) context.getJobDetail().getJobDataMap().get("trayIcon");
		if (jobException!=null) {
			logger.error("XXXXXX 任务调度出现异常  : "+jobName+" 异常信息 : "+jobException.getMessage());
			logger.error("XXXXXX 邮件发送失败!");
			trayIcon.displayMessage("错误", "邮件发送失败!",TrayIcon.MessageType.ERROR);
			//此处应添加 失败发送邮件通知
			
		}else {
			logger.info("@@@@@@ 邮件发送成功!");
			trayIcon.displayMessage("提示", "邮件发送成功!",TrayIcon.MessageType.INFO);
		}
		
	}

}
