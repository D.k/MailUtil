package me.mritd.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
/**
 * 
 * Copyright © 2015 Mritd. All rights reserved.
 * @ClassName: FileUtil
 * @Description: TODO 稳健操作工具
 * @author: 漠然
 * @date: 2015年10月31日 下午8:09:30
 * @version 1.0
 */
public class FileUtil {
	
	private final Logger logger  = Logger.getLogger(FileUtil.class);
	
	/**
	 * 
	 * @Title getListFiles 
	 * @Description TODO 根据文件夹或文件夹路径递归搜索所有文件
	 * @param obj
	 * @return ArrayList<File>
	 * @throws
	 */
	public ArrayList<File> getListFiles(Object obj) {
		
		File directory = null;
		ArrayList<File> files = new ArrayList<File>();
		
		try {
			if (obj instanceof File) {
				directory = (File) obj;
			} else {
				directory = new File(obj.toString());
			} 
			if (directory.isFile()) {
				files.add(directory);
				return files;
			} else if (directory.isDirectory()) {
				File[] fileArr = directory.listFiles();
				for (int i = 0; i < fileArr.length; i++) {
					File fileOne = fileArr[i];
					files.addAll(getListFiles(fileOne));
				}
			}
			return files;
		} catch (Exception e) {
			logger.error("###### 文件列表获取失败: ",e);
			return files;
		}
	}
	
	/**
	 * 
	 * @Title getLastModifyFile 
	 * @Description TODO 对指定ArrayList中文件按照修改时间排序，并返回最后修改文件
	 * @param ArrayList<File> files
	 * @return File file
	 * @throws
	 */
	public File getLastModifyFile(ArrayList<File> files){
		logger.info("######  开始对文件列表排序!");
		HashMap<Long,File> fileMap = new HashMap<Long,File>();
		for (int i = 0; i < files.size(); i++) {
			File tmpFile = files.get(i);
			fileMap.put(tmpFile.lastModified(),tmpFile);
		}
		List<Map.Entry<Long,File>> infoIds = new ArrayList<Map.Entry<Long,File>>(fileMap.entrySet()); 
		// 对HashMap中的key 进行排序  
        Collections.sort(infoIds, new Comparator<Map.Entry<Long,File>>() {  
            public int compare(Map.Entry<Long,File> o1,Map.Entry<Long,File> o2) {  
                return (o1.getKey()).toString().compareTo(o2.getKey().toString());  
            }  
        });
        logger.info("######  HashMap文件列表排序完成!");
        logger.info("###### 文件列表长度 : "+infoIds.size());
        logger.info("###### 文件列表 : "+infoIds.toString());
        logger.info("###### 最后修改文件: "+infoIds.get(infoIds.size()-1).getValue());
        return infoIds.get(infoIds.size()-1).getValue();
	}
	
	/**
	 * 
	 * @Title readFileByLines 
	 * @Description TODO 以行为单位读取文件
	 * @param File file
	 * @return String
	 * @throws 
	 */
	public String readFileByLines(File file) {
		BufferedReader reader = null;
		StringBuffer buffer = new StringBuffer();
		try {
			logger.info("######  以行为单位读取文件内容 --->"+file.getName());
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null)
				buffer.append(tempString);
			logger.info("######  文件内容 : "+buffer);
			reader.close();
		} catch (IOException e) {
			logger.error("###### 获取HTML文件内容失败!");
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					logger.error("######  reader 关闭失败");
				}
			}
		}
		return buffer.toString();
	}
	
}
