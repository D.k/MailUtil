package me.mritd.util;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import me.mritd.quartz.execute.EmailJobExecute;
import me.mritd.swing.MailFrame;

import org.apache.log4j.Logger;
import org.quartz.Trigger.TriggerState;

/**
 * 
 * @ClassName schedulerListener 
 * @Description TODO 轮询监测调度器状态
 * @author 漠然  mritd@mritd.me
 * @date 2015年10月6日 下午7:48:03 
 * @version 1.0
 */
public class QuartzStatusListener extends Thread {
	
	private JLabel QuartzStatus;
	private JLabel EmailJobStatus;
	private final Logger logger = Logger.getLogger(QuartzStatusListener.class);
	
	@Override
	public void run() {
		EmailJobExecute emailJobExecute = new EmailJobExecute();
		try {
			while (true) {
				boolean schedulerState = emailJobExecute.schedulerStatus();
				TriggerState emailJobState = emailJobExecute.emailJobStatus();
				if (schedulerState) {
					QuartzStatus.setText("运行中");
					QuartzStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_start.png")));
				} else {
					QuartzStatus.setText("已停止");
					QuartzStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_stop.png")));
				}
				
				/**
				 * 
				 * None：Trigger已经完成，且不会在执行，或者找不到该触发器，或者Trigger已经被删除
				 * NORMAL:正常状态
				 * PAUSED：暂停状态
				 * COMPLETE：触发器完成，但是任务可能还正在执行中
				 * BLOCKED：线程阻塞状态
				 * ERROR：出现错误
				 * 
				 */
				switch (emailJobState) {
				case NONE:
					EmailJobStatus.setText("已停止");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_stop.png")));
					break;
				case NORMAL:
					EmailJobStatus.setText("状态正常");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_normal.png")));
					break;
				case PAUSED:
					EmailJobStatus.setText("任务暂停");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_paused.png")));
					break;
				case COMPLETE:
					EmailJobStatus.setText("运行中");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_start.png")));
					break;
				case BLOCKED:
					EmailJobStatus.setText("任务锁定状态");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_joblock.png")));
					break;
				case ERROR:
					EmailJobStatus.setText("任务暂停");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_joberror.png")));
					break;

				default:
					EmailJobStatus.setText("已停止");
					EmailJobStatus.setIcon(new ImageIcon(MailFrame.class
							.getResource("/me/mritd/img/menu_stop.png")));
					break;
				}
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			logger.error("异常信息: ", e);
		}

	}

	public QuartzStatusListener(JLabel QuartzStatus,JLabel EmailJobStatus) {
		this.QuartzStatus=QuartzStatus;
		this.EmailJobStatus=EmailJobStatus;
	}
}
