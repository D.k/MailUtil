package me.mritd.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * Copyright © 2015 Mritd. All rights reserved.
 * @ClassName: ParamUtil
 * @Description: TODO 配置文件参数读取类
 * @author: 漠然
 * @date: 2015年10月31日 下午8:13:28
 * @version 1.0
 */
public class ParamUtil {
	
	/**
	 * 
	 * @ClassName LoadPath 
	 * @Description TODO 配置文件路径泛型 
	 * @author 漠然  mritd@mritd.me
	 * @date 2015年10月5日 下午12:23:27 
	 * @version 1.0
	 */
	public enum LoadPathType{
		CLASSPATH,ROOTPATH;
	}
	
	private Logger logger = Logger.getLogger(ParamUtil.class);
	/**
	 * 
	 * @Title: loadParam
	 * @Description: TODO 读取classpath下配置文件
	 * @param fileName
	 *            配置文件
	 * @param ParamUtil.LoadPathType
	 *            配置文件路径类型(classpath/绝对路径)
	 * @return Properties 返回值类型
	 * @throws
	 */
	public Properties loadParam(String filePath,ParamUtil.LoadPathType pathType) {
		Properties properties = null;
		InputStream input = null;
		try {
			if (!filePath.endsWith(".properties")) {
				logger.error("###### 传入的文件不是properties配置文件!");
				return properties;
			}
			properties = new Properties();
			if(pathType==ParamUtil.LoadPathType.CLASSPATH){
				// 路径处理
				if (!filePath.startsWith("/"))
					filePath = "/" + filePath;
				// 加载Java项目根路径下的配置文件
				input = ParamUtil.class.getResourceAsStream(filePath);
			}else if(pathType==ParamUtil.LoadPathType.ROOTPATH){
				// 加载配置文件 绝对路径
				input = new FileInputStream(filePath);
			}
			// 加载属性文件
			properties.load(input);
		} catch (Exception e) {
			logger.error("###### 加载配置文件出错!!!");
			logger.error("异常信息: ", e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.error("关闭IO流失败: ", e);
				}
			}
		}
		return properties;
	}
	
	/**
	 * 
	 * @Title writeProperties 
	 * @Description TODO 配置文件写入方法
	 * @param key 
	 * @param value
	 * @param comment
	 * @param filePath
	 * @param ParamUtil.LoadPathType
	 * @return boolean
	 * @throws FileNotFoundException
	 * @throws IOException 
	 */
	public boolean writeProperties(Map<String,String> data,String comment, String filePath,ParamUtil.LoadPathType pathType) throws FileNotFoundException,
			IOException {
		Properties properties = new Properties();
		FileOutputStream fos = null;
		try {
			if (pathType==ParamUtil.LoadPathType.CLASSPATH) {
				// 路径处理
				if (!filePath.startsWith("/"))
					filePath = "/" + filePath;
				File file = new File(ParamUtil.class.getResource(filePath).toURI());
				if (file.exists()) {
					FileInputStream fis = new FileInputStream(file);
					properties.load(fis);
					fis.close();
				}
				fos = new FileOutputStream(file);
			} else if(pathType==ParamUtil.LoadPathType.ROOTPATH){
				File file = new File(filePath);
				if (file.exists()) {
					FileInputStream fis = new FileInputStream(file);
					properties.load(fis);
					fis.close();
				}
				fos = new FileOutputStream(new File(filePath));
			}
			//遍历Map 加载数据
			if (data!=null) {
				for (Map.Entry<String, String> entry : data.entrySet()) {  
					logger.info("###### 写入配置 --> key:"+entry.getKey()+" value:"+entry.getValue()+" comment:"+comment);
					properties.setProperty(entry.getKey(), entry.getValue());
				}  
				properties.store(fos, comment);
			}else {
				logger.info("###### 传入的参数Map为空!");
				return false;
			}
		} catch (URISyntaxException e) {
			logger.error("###### 获取CLASSPATH下配置文件URI失败！");
			logger.error("###### 配置文件写入失败！");
			logger.error("异常信息: ", e);
			return false;
		} catch (FileNotFoundException e) {
			logger.error("###### 写入时创建输出流失败！");
			logger.error("###### 配置文件写入失败！");
			logger.error("异常信息: ", e);
		}
		return true;
	}
}


