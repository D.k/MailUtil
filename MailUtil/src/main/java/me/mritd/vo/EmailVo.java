package me.mritd.vo;

import java.io.File;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Properties;

/**
 * 
 * @ClassName Email
 * @Description TODO 邮件信息封装类
 * @author 漠然 mritd@mritd.me
 * @date 2015年10月5日 上午9:14:14
 * @see <a href='http://www.xdemo.org/java-mail/'>大飞的xdemo站点<a>
 */
public class EmailVo {
	// 发送邮件的服务器(IP或网址)
	private String mailServerHost;
	// 发送邮件服务器端口
	private String mailServerPort;
	// 邮件发送者的地址
	private String fromAddress;
	// 邮件发送者名称
	private String fromNickName;
	// 邮件接收者的地址
	private String toAddress;
	// 登陆邮件发送服务器的用户名和密码
	private String userName;
	private String password;
	// 是否需要身份验证
	private boolean validate = false;
	// 邮件主题
	private String subject;
	// 邮件的文本内容
	private String content;
	// 邮件附件的文件名
	private List<File> attachments;
	//HTML目录位置 用于Job Listener更新
	private String htmlDir;

	/**
	 * 获得邮件会话属性
	 * 
	 * @throws GeneralSecurityException
	 */
	public Properties getProperties() throws GeneralSecurityException {
		Properties props = new Properties();
		props.put("mail.smtp.host", this.mailServerHost);
		props.put("mail.smtp.port", this.mailServerPort);
		props.put("mail.smtp.auth", validate ? "true" : "false");
		// SSL
		/*
		 * MailSSLSocketFactory sf = new MailSSLSocketFactory();
		 * sf.setTrustAllHosts(true); props.put("mail.smtp.ssl.enable", "true");
		 * props.put("mail.smtp.ssl.socketFactory", sf);
		 * props.put("mail.smtp.socketFactory.fallback", "false");
		 * props.setProperty("mail.transport.protocol", "smtps");
		 * props.put("mail.smtp.starttls.enable", "true");
		 */
		return props;
	}
	
	/**
	 * 
	 * @Title cleanInfo
	 * @Description TODO 清空邮件发送信息
	 * @return void
	 */
	public void cleanInfo(){
		this.mailServerHost = null;
		this.mailServerPort = null;
		this.fromAddress = null;
		this.fromNickName = null;
		this.toAddress = null;
		this.userName = null;
		this.password = null;
		this.validate = false;
		this.subject = null;
		this.content = null;
		this.attachments = null;
		this.htmlDir = null;
	}

	
	public EmailVo(String mailServerHost, String mailServerPort, String fromAddress, String fromNickName,
			String toAddress, String userName, String password, boolean validate, String subject, String content,
			List<File> attachments, String htmlDir) {
		super();
		this.mailServerHost = mailServerHost;
		this.mailServerPort = mailServerPort;
		this.fromAddress = fromAddress;
		this.fromNickName = fromNickName;
		this.toAddress = toAddress;
		this.userName = userName;
		this.password = password;
		this.validate = validate;
		this.subject = subject;
		this.content = content;
		this.attachments = attachments;
		this.htmlDir = htmlDir;
	}

	public EmailVo() {
		super();
	}

	public String getMailServerHost() {
		return mailServerHost;
	}

	public void setMailServerHost(String mailServerHost) {
		this.mailServerHost = mailServerHost;
	}

	public String getMailServerPort() {
		return mailServerPort;
	}

	public void setMailServerPort(String mailServerPort) {
		this.mailServerPort = mailServerPort;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}

	public List<File> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<File> attachments) {
		this.attachments = attachments;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String textContent) {
		this.content = textContent;
	}

	public String getFromNickName() {
		return fromNickName;
	}

	public void setFromNickName(String fromNickName) {
		this.fromNickName = fromNickName;
	}

	public String getHtmlDir() {
		return htmlDir;
	}

	public void setHtmlDir(String htmlDir) {
		this.htmlDir = htmlDir;
	}
	
	
}
